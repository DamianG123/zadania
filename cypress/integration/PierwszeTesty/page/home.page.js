class homePage {
    constructor() {
        this.elements = {
            singleDressFoundBySearch: 'li[class="ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line first-item-of-mobile-line"]'
            
        }
        this.testData = {
            dresses: [
                {quantity: 3, color: 'Pink', size: 'M'},
                {quantity: 7, color: 'Beige', size: 'L'}
              ]
        }
    }
    login(){
        cy.get('a[class="login"]').should("be.visible").click()
    }
    addDressFoundBySerach(){
        //1. Wyszukać sukienek
        cy.get('input[id="search_query_top"]').type("dress");
        cy.get('#searchbox .btn').click();
        //2. Najechać na wybrany produkt
        cy.get(this.elements.singleDressFoundBySearch).trigger('mouseover')
        cy.get('div[class="button-container"] a span').should('have.length.at.least', 1)
        //3. Nacisnąć przycisk dodaj do koszyka
        cy.get('a[title="Add to cart"] span').contains('Add to cart').should('be.visible').click();
        //4. 
        cy.get('span[class="cross"]').click();
        cy.get('div[class="shopping_cart"]').should('not.include.text', 'Empty')
    }
    addDressFoundBySubmenu(){
        //Wyszukanie właściwej sukienki
        cy.get('[title="Women"]:eq(0)').parent().trigger('mouseover')
        cy.get('.submenu-container').invoke('show')
        cy.get('[title="Evening Dresses"]:eq(0)').invoke('show').click()
        cy.get('img[title="Printed Dress"').click()
    }
    addToCart(){
        //Dodanie sukienki do koszyka
        cy.get('button[type="submit"]').contains('Add to cart').click()
        cy.get('span.continue').click()
    }
    addMenyDresses(){
        this.testData.dresses.forEach(dress => {
            cy.get('#quantity_wanted').clear()
            cy.get('#quantity_wanted').type(dress.quantity);
            cy.get(`[name=${dress.color}]`).click();
            cy.get(`[name='group_1']`).select(dress.size);
            this.addToCart()
        })
    }
    removeOneDressFromEachSize(){
        cy.get('[title="View my shopping cart"]').click()
        cy.get('[title="Subtract"]:eq(0)').click();
        cy.get('[title="Subtract"]:eq(1)').click();
        cy.get('[title="Subtract"]:eq(2)').click();
    }
    addToWishList(){
        //Wejdź na przeceniony produkt
        cy.get('#homefeatured > .last-mobile-line > .product-container > .right-block > .button-container > .lnk_view > span').click()
        //Kliknij aby dodać produkt do listy życzeń
        cy.get('#wishlist_button').click()
        //Sprawdź czy wyświetliło się okienko informujące o dodaniu produktu do listy życzeń
        cy.get('.fancybox-error').should('contain.text', 'Added to your wishlist.')        
    }
}

export default homePage