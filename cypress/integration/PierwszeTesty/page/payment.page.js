class paymentPage {
    constructor() {
        this.elements = {

        }
        this.testData = {

        }
    }
    goToCart(){
        cy.get('div[class="shopping_cart"] a b').click()
        cy.get('p[class="alert alert-warning"]').should('not.be.visible')
    }
    pay(){
        cy.get('a[title="Proceed to checkout"]:eq(1)').click()
        cy.get('button[name="processAddress"]').click()
        cy.get('input[type="checkbox"]').click()
        cy.get('button[name="processCarrier"]').click()
        cy.get('a[class="bankwire"]').click()
        cy.get('button[type="submit"]:eq(1)').click()
        cy.url().should('include', 'http://automationpractice.com/index.php?controller=order-confirmation')
    }


}
export default paymentPage