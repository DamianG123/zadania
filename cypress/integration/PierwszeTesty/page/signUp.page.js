class singUpPage {
    constructor() {
        this.elements = {     
        }
        this.testData = {
        }
    }

    fillForm() {
        const faker = require("faker")
        cy.url({timeout: 20000}).should("eq", "http://automationpractice.com/index.php?controller=authentication&back=my-account#account-creation")
        cy.get('#id_gender1').click()
        cy.get('#customer_firstname').type(faker.name.firstName())
        cy.get('#customer_lastname').type(faker.name.lastName())
        cy.readFile('cypress/integration/Pierwszetesty/config.json').then((element)=>{
            let password=element.password
            cy.get('#passwd').type(password)
          })
        cy.get('#days').select("6")
        cy.get('#months').select("7")
        cy.get('#years').select("2020")
        cy.get('#company').type(faker.company.companyName())
        cy.get('#address1').type(faker.address.streetAddress())
        cy.get('#city').type(faker.address.cityName())
        cy.get('div[id="uniform-id_state"]').click()
        cy.get('#id_country').select("21")
        cy.get('#id_state').select("5")
        // cy.get('#postcode').type(faker.address.zipCode())
        cy.get('#postcode').type("01234")
        cy.get('#other').type(faker.lorem.sentence())
        // cy.get('#phone_mobile').type(faker.phone.phoneNumber())
        cy.get('#phone_mobile').type("123456789")
        cy.get('#alias').clear()
        cy.get('#alias').type(faker.address.streetAddress())
        cy.get('#submitAccount > span').click()
        cy.url({timeout: 20000}).should("eq", "http://automationpractice.com/index.php?controller=my-account")
    }
    logout() {
        return cy.get('.logout')
    }
}
export default singUpPage