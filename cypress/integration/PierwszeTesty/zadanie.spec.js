///<reference types="cypress"/>
import homePage from './page/home.page'
import signPage from './page/sign.page'
import singUpPage from './page/signUp.page'
import paymentPage from './page/payment.page'

const faker = require("faker")
const dresses = [
  {quantity: 3, color: 'Pink', size: 'M'},
  {quantity: 7, color: 'Beige', size: 'L'}
]

describe('My First Test', () => {
  let SignUpPage = new singUpPage()
  let HomePage = new homePage()
  let SignPage = new signPage()
  let PaymentPage = new paymentPage()
  before('Rejestracja',() => {
    cy.visit('http://automationpractice.com/index.php')
    //Kliknąć w "sing in"
    HomePage.login()
    cy.url().should("eq", "http://automationpractice.com/index.php?controller=authentication&back=my-account")
    //W sekcji Create Accunt podać maila i kliknąć przycisc creat account
    SignPage.createEmailandPassword()
    SignPage.submitSignUp()
    //Wypełnić formularz
    SignUpPage.fillForm()
    //Wyloguj się
    SignUpPage.logout().click()
    })

  beforeEach('Logowanie',() => {
    cy.visit('http://automationpractice.com/index.php')
    //Kliknąć w "sign in"
    HomePage.login()
    //Zaloguj się
    SignPage.signIn()
    cy.visit('http://automationpractice.com/index.php')
  })
  
  it('Add dress and pay', () => {
    //Find and add Dress, next go to cart
    HomePage.addDressFoundBySerach()
    //Sprawdzić czy sukienka została dodana do koszyka
    PaymentPage.goToCart()    
    //6.Przejdź proces płatności
    PaymentPage.pay()
  })

  it(`Add many dresses to cart`, () => {
    //Znajdź i dodaj do koszyka sukienkę
    HomePage.addDressFoundBySubmenu()
    HomePage.addToCart()
    //Dodanie kolejnych sztuk tej samej sukienki do koszyka
    HomePage.addMenyDresses()
    //Usuwanie po jednej sukience z każdego rozmiaru
    HomePage.removeOneDressFromEachSize()
})
  it('Add to wishlist', () => {
    HomePage.addToWishList()
  })
})